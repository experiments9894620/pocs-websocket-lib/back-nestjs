import {
  HttpException,
  HttpStatus,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { AuthService } from './auth/services/auth.service';
import { NextFunction, Response } from 'express';
import { User } from './panier/entities/user.entity';
import { UserService } from './panier/user.service';

export interface RequestModel {
  user: string;
  headers: any;
}

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {}

  async use(request: RequestModel, response: Response, next: NextFunction) {
    try {
      const tokenArray: string[] = request.headers['authorization'].split(' ');
      // throws if the token is not valid
      const decodedToken = await this.authService.verifyJwt(tokenArray[1]);

      // make sure that the user is not delete
      // or that props changed during the time that the jwt was issued to the user
      const user: User = await this.userService.findOneByUserName(
        decodedToken.user.id,
      );

      if (user !== undefined) {
        // attach the user object to our request object - so we can access it any time later in our application
        // if it would be here, we would overwrite it
        request.user = user.name;
        next();
      } else {
        throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
      }
    } catch {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }
}
