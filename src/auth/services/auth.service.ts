import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  verifyJwt(jwt: string) {
    return this.jwtService.verify(jwt);
  }
}
