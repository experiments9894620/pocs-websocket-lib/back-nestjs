import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Article } from './entities/article.entity';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article)
    private articleRepository: Repository<Article>,
  ) {}

  async save(article: Article) {
    return await this.articleRepository.save(article);
  }

  findAllForUser(name: string): Promise<Article[]> {
    return this.articleRepository.findBy({ user: { name } });
  }

  remove(id: number) {
    return this.articleRepository.delete({ id });
  }
}
