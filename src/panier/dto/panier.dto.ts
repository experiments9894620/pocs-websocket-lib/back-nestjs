import { ArticleDto } from './article.dto';

export class PanierDto {
  articleListe: ArticleDto[];
  prixTotal: number;
}
