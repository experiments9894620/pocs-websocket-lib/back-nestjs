import { Article } from '../entities/article.entity';
import { PanierDto } from '../dto/panier.dto';
import { ArticleDto } from '../dto/article.dto';
import { CreateArticleDto } from '../dto/create-article.dto';
import { User } from '../entities/user.entity';

export class Mappers {
  static toPanierDto(articleListe: Article[]): PanierDto {
    return {
      articleListe: (articleListe ?? []).map((article) =>
        this.toArticleDto(article),
      ),
      prixTotal: (articleListe ?? [])
        .map((article) => article.prix * article.quantite)
        .reduce(
          (previousValue, currentValue) => previousValue + currentValue,
          0,
        ),
    } as PanierDto;
  }

  static toArticleDto(article: Article): ArticleDto {
    return { ...article } as ArticleDto;
  }

  static toArticle(articleDto: CreateArticleDto, user: User): Article {
    return {
      id: null,
      ...articleDto,
      user,
    };
  }
}
