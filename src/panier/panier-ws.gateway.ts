import {
  OnGatewayConnection,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Namespace, Server } from 'socket.io';
import { ArticleService } from './article.service';
import { Mappers } from './mappers/mappers';
import { SocketWithAuth } from '../socket-io-adapter';

@WebSocketGateway(3101, { namespace: 'paniers', cors: { origin: '*' } })
export class PanierWsGateway implements OnGatewayInit, OnGatewayConnection {
  private logger: Logger = new Logger('PanierWsGateway');

  @WebSocketServer() server: Namespace;

  constructor(private readonly articleService: ArticleService) {}

  afterInit(server: Server) {
    this.logger.debug('Initialized');
  }

  async handleConnection(socket: SocketWithAuth) {
    const sockets = this.server.sockets;
    this.logger.debug('handleConnection');
    this.logger.debug(`Number of connected sockets: ${sockets.size}`);
    // if the token is not verified, this will throw and we can catch & disconnect the user

    await socket.join(socket.userID);

    // Only emit todos to the specific connected client
    return await this.emitPanierForUser(socket.userID);
  }

  async emitPanierForUser(username: string) {
    this.logger.log('emitPanierForUser', username);
    const articlesListe = await this.articleService.findAllForUser(username);

    return this.server
      .to(username)
      .emit('panier', Mappers.toPanierDto(articlesListe));
  }
}
