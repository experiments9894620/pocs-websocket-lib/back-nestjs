import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Article } from './article.entity';
import { Projet } from './projet.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Article, (article) => article.user, {
    cascade: true,
  })
  articleListe: Article[];

  @ManyToMany(() => Projet, (projet) => projet.userListe)
  @JoinTable()
  projetListe: Projet[];
}
