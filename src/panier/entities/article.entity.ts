import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Article {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  prix: number;

  @ManyToOne(() => User, (user) => user.articleListe)
  user: User;

  @Column({ default: 1 })
  quantite: number;
}
