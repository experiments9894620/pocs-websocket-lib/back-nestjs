import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity({
  synchronize: false,
})
export class Projet {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => User, (user) => user.projetListe)
  userListe: User[];

  @Column()
  reference: number;
}
