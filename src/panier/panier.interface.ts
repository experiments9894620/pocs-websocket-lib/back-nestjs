export interface ConnectionI {
  id?: number;
  socketId?: string;
  connectedUser?: string;
}
