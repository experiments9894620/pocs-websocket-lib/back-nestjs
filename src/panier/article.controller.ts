import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ArticleService } from './article.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { UserService } from './user.service';
import { Mappers } from './mappers/mappers';
import { PanierWsGateway } from './panier-ws.gateway';

@Controller('v1/articles')
export class ArticleController {
  constructor(
    private readonly articleService: ArticleService,
    private readonly userService: UserService,
    private readonly panierWsGateway: PanierWsGateway,
  ) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  async create(@Body() createArticleDto: CreateArticleDto, @Request() req) {
    const user = await this.userService.findOneByUserName(req.user.id);
    const article = await this.articleService.save(
      Mappers.toArticle(createArticleDto, user),
    );

    await this.panierWsGateway.emitPanierForUser(user.name);

    return Mappers.toArticleDto(article);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  async find(@Request() req) {
    return Mappers.toPanierDto(
      await this.articleService.findAllForUser(req.user.id),
    );
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  async delete(@Param('id') id, @Request() req) {
    await this.articleService.remove(+id);

    await this.panierWsGateway.emitPanierForUser(req.user.id);

    return;
  }
}
