import { Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { PanierWsGateway } from './panier-ws.gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Article } from './entities/article.entity';
import { AuthModule } from '../auth/auth.module';
import { User } from './entities/user.entity';
import { UserService } from './user.service';
import { ArticleController } from './article.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Article, User]), AuthModule],
  controllers: [ArticleController],
  providers: [ArticleService, PanierWsGateway, UserService],
  exports: [TypeOrmModule],
})
export class PanierModule {}
