import { INestApplication, Logger, WebSocketAdapter } from '@nestjs/common';
import { Server, ServerOptions } from 'socket.io';
import { Socket } from 'socket.io';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { AuthService } from './auth/services/auth.service';
import { User } from './panier/entities/user.entity';
import { UserService } from './panier/user.service';

// guard types
export type AuthPayload = {
  userID: string;
  contextApp: string;
};

export type SocketWithAuth = Socket & AuthPayload;

export class SocketAdapter extends IoAdapter implements WebSocketAdapter {
  logger = new Logger(SocketAdapter.name);
  constructor(private app: INestApplication) {
    super(app);
  }

  createIOServer(port: number, options?: ServerOptions) {
    const authService = this.app.get(AuthService);
    const userService = this.app.get(UserService);
    const server: Server = super.createIOServer(port, options);

    server
      .of('paniers')
      .use(createTokenMiddleware(authService, userService, this.logger));

    return server;
  }
}

const createTokenMiddleware =
  (authService: AuthService, userService: UserService, logger: Logger) =>
  async (socket: SocketWithAuth, next) => {
    // for Postman testing support, fallback to token header
    const token =
      socket.handshake.auth.token || socket.handshake.headers['token'];

    logger.debug(`Validating auth token before connection: ${token}`);

    try {
      const decodedToken = authService.verifyJwt(token);
      socket.userID = decodedToken.user.id;
      const user: User = await userService.findOneByUserName(socket.userID);
      console.log(user);
      if (user !== undefined && user !== null) {
        logger.log(`utilisateur connu`);
        next();
      } else {
        logger.log(`utilisateur inconnu`);
        next(new Error('USER NOT FOUND'));
      }
    } catch {
      next(new Error('FORBIDDEN'));
    }
  };
