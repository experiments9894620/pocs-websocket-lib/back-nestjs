import { config } from 'dotenv';
import { DataSource, DataSourceOptions } from 'typeorm';
import { DatabasePostgresConfiguration } from './database-postgres.configuration';

// This config file is used from TypeORM CLI to fetch DB configuration

config(); // load environment variables

const datasourceOptions =
  new DatabasePostgresConfiguration().createTypeOrmOptions() as DataSourceOptions;
export default new DataSource(datasourceOptions);
