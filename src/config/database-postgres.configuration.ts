import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { join } from 'path';

export class DatabasePostgresConfiguration implements TypeOrmOptionsFactory {
  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'poc_si_commande',
      password: 'poc_si_commande',
      database: 'poc_si_commande',
      logging: 'all',
      autoLoadEntities: true,
      synchronize: false,
      entities: [join(__dirname, '../**/*.entity{.ts,.js}')],
      migrationsRun: true,
      migrations: [join(__dirname, '../../migrations/*{.ts,.js}')],
    };
  }
}
