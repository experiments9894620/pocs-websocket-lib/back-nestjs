import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { SocketAdapter } from './socket-io-adapter';

async function bootstrap() {
  const logger = new Logger('Main (main.ts)');

  const port = 3100;
  const app = await NestFactory.create(AppModule, {
    logger: ['verbose'],
    cors: true,
  });

  app.setGlobalPrefix('api');
  app.useWebSocketAdapter(new SocketAdapter(app));
  await app.listen(port);

  logger.log(`Server running on port ${port}`);
}
bootstrap();
