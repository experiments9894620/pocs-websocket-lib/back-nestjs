# POC 

## Migration

### Problématique 
Ne faire la migration que sur les table du projet et non sur les tables déjà éxistante 


### Solution
Avant tout il faut avoir defini les entitées des anciennes tables,
puis génére les migrations, depuis la base de donnée contenant deja les anciennes tables, si les entitées sont bien définie, la migration ne devrait pas contenir les anciennes tables.

### Validation
Pour valider que la migration ne fais la migration que de ce qui est nouveau, il faut executer le script sql suivant :

```sql
create table projet
(
    id   int auto_increment
        primary key,
    name varchar(255) not null
);
``` 

Puis executer la commande suivante :

```shell script
npm run migration:generate -- migrations/add-reference
```